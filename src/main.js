import Vue from 'vue'
import App from './App.vue'
import router from './router'

// Plugins
import '@/plugins/quasar'

// Global styles
// import '@/styles/quasar.scss'

// Components
import IconsSet from "@/components/elements/icons/IconsSet"
Vue.component('icons-set', IconsSet)

// Vue.config.productionTip = false
Vue.config.devtools = true

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
