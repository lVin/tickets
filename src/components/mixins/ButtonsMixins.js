import IconsSet from "components/elements/icons/IconsSet";

export const ButtonsMixins = {

    components: {
        IconsSet,
    },

    props: {
        label: {require: true},
        icon: String,
        positive: Boolean,
        positive_disabled: Boolean,
        secondary: Boolean,
        secondary_disabled: Boolean,
        ghost: Boolean,
        ghost_disabled: Boolean,
        disabled: Boolean
    },

    computed: {
        hasText() {
            return !!(this.$slots.default || this.label)
        },
        mainClass() {
            return {
                'positive': this.positive,
                'positive_disabled': this.positive_disabled,
                'secondary': this.secondary,
                'secondary_disabled': this.secondary_disabled,
                'ghost': this.ghost,
                'ghost_disabled': this.ghost_disabled,
                'disabled': this.disabled
            }
        },

    },
    methods: {
        clickHandler() {
            if (!this.disabled)
                this.to ? this.$router.push(this.to) : this.$emit('click')
        }
    }
}

