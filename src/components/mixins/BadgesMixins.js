export const BadgesMixins = {
    props: {
        label: {
          type: String,
          default: "",
        },
    
        pink: Boolean,
        jade: Boolean,
        purple: Boolean,
        success: Boolean,
        warning: Boolean,
        negative: Boolean,
        greyscale300: Boolean,
        greyscale500: Boolean,
        greyscale900: Boolean,
      },
    
      computed: {
        mainClass() {
          return {
            pink: this.pink,
            jade: this.jade,
            purple: this.purple,
            success: this.success,
            warning: this.warning,
            negative: this.negative,
            greyscale300: this.greyscale300,
            greyscale500: this.greyscale500,
            greyscale900: this.greyscale900,
          };
        },
      },
}

