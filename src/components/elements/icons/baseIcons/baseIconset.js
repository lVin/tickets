const baseIconset = {

    chatFilled: () => import('components/elements/icons/baseIcons/svg/filled/chatFilled'),
    heartFilled: () => import('components/elements/icons/baseIcons/svg/filled/heartFilled'),
    chatDottedFilled: () => import('components/elements/icons/baseIcons/svg/filled/chatDottedFilled'),
    heartCrossedFilled: () => import('components/elements/icons/baseIcons/svg/filled/heartCrossedFilled'),
    homeFilled: () => import('components/elements/icons/baseIcons/svg/filled/homeFilled'),
    gamepadFilled: () => import('components/elements/icons/baseIcons/svg/filled/gamepadFilled'),
    musicFilled: () => import('components/elements/icons/baseIcons/svg/filled/musicFilled'),
    messageFilled: () => import('components/elements/icons/baseIcons/svg/filled/messageFilled'),
    starFilled: () => import('components/elements/icons/baseIcons/svg/filled/starFilled'),
    volumeDownFilled: () => import('components/elements/icons/baseIcons/svg/filled/volumeDownFilled'),
    dribbleFilled: () => import('components/elements/icons/baseIcons/svg/filled/dribbleFilled'),
    settingsFilled: () => import('components/elements/icons/baseIcons/svg/filled/settingsFilled'),
    singleUserFilled: () => import('components/elements/icons/baseIcons/svg/filled/singleUserFilled'),

    tagsOutline: () => import('components/elements/icons/baseIcons/svg/outline/tagsOutline'),
    chatOutline: () => import('components/elements/icons/baseIcons/svg/outline/chatOutline'),
    heartOutline: () => import('components/elements/icons/baseIcons/svg/outline/heartOutline'),
    chatDottedOutline: () => import('components/elements/icons/baseIcons/svg/outline/chatDottedOutline'),
    heartCrossedOutline: () => import('components/elements/icons/baseIcons/svg/outline/heartCrossedOutline'),
    homeOutline: () => import('components/elements/icons/baseIcons/svg/outline/homeOutline'),
    gamepadOutline: () => import('components/elements/icons/baseIcons/svg/outline/gamepadOutline'),
    musicOutline: () => import('components/elements/icons/baseIcons/svg/outline/musicOutline'),
    messageOutline: () => import('components/elements/icons/baseIcons/svg/outline/messageOutline'),
    starOutline: () => import('components/elements/icons/baseIcons/svg/outline/starOutline'),
    volumeDownOutline: () => import('components/elements/icons/baseIcons/svg/outline/volumeDownOutline'),
    dribbleOutline: () => import('components/elements/icons/baseIcons/svg/outline/dribbleOutline'),
    settingsOutline: () => import('components/elements/icons/baseIcons/svg/outline/settingsOutline'),
    singleUserOutline: () => import('components/elements/icons/baseIcons/svg/outline/singleUserOutline'),

    attentionFilled: () => import('components/elements/icons/baseIcons/svg/filled/attentionFilled'),
    lockFilled: () => import('components/elements/icons/baseIcons/svg/filled/lockFilled'),
    unlockFilled: () => import('components/elements/icons/baseIcons/svg/filled/unlockFilled'),
    videoFilled: () => import('components/elements/icons/baseIcons/svg/filled/videoFilled'),
    locationFilled: () => import('components/elements/icons/baseIcons/svg/filled/locationFilled'),
    deleteFilled: () => import('components/elements/icons/baseIcons/svg/filled/deleteFilled'),
    blitzFilled: () => import('components/elements/icons/baseIcons/svg/filled/blitzFilled'),
    exchangeFilled: () => import('components/elements/icons/baseIcons/svg/filled/exchangeFilled'),
    realLifeFilled: () => import('components/elements/icons/baseIcons/svg/filled/realLifeFilled'),
    gifFilled: () => import('components/elements/icons/baseIcons/svg/filled/gifFilled'),
    playFilled: () => import('components/elements/icons/baseIcons/svg/filled/playFilled'),
    headPhonesFilled: () => import('components/elements/icons/baseIcons/svg/filled/headPhonesFilled'),
    diceFilled: () => import('components/elements/icons/baseIcons/svg/filled/diceFilled'),

    attentionOutline: () => import('components/elements/icons/baseIcons/svg/outline/attentionOutline'),
    lockOutline: () => import('components/elements/icons/baseIcons/svg/outline/lockOutline'),
    unlockOutline: () => import('components/elements/icons/baseIcons/svg/outline/unlockOutline'),
    videoOutline: () => import('components/elements/icons/baseIcons/svg/outline/videoOutline'),
    locationOutline: () => import('components/elements/icons/baseIcons/svg/outline/locationOutline'),
    deleteOutline: () => import('components/elements/icons/baseIcons/svg/outline/deleteOutline'),
    blitzOutline: () => import('components/elements/icons/baseIcons/svg/outline/blitzOutline'),
    exchangeOutline: () => import('components/elements/icons/baseIcons/svg/outline/exchangeOutline'),
    realLifeOutline: () => import('components/elements/icons/baseIcons/svg/outline/realLifeOutline'),
    gifOutline: () => import('components/elements/icons/baseIcons/svg/outline/gifOutline'),
    playOutline: () => import('components/elements/icons/baseIcons/svg/outline/playOutline'),
    headPhonesOutline: () => import('components/elements/icons/baseIcons/svg/outline/headPhonesOutline'),
    diceOutline: () => import('components/elements/icons/baseIcons/svg/outline/diceOutline'),

    optionFilled: () => import('components/elements/icons/baseIcons/svg/filled/optionFilled'),
    optionVerticalFilled: () => import('components/elements/icons/baseIcons/svg/filled/optionVerticalFilled'),
    activityFilled: () => import('components/elements/icons/baseIcons/svg/filled/activityFilled'),
    calendarFilled: () => import('components/elements/icons/baseIcons/svg/filled/calendarFilled'),
    cancelFilled: () => import('components/elements/icons/baseIcons/svg/filled/cancelFilled'),
    searchFilled: () => import('components/elements/icons/baseIcons/svg/filled/searchFilled'),
    notificationFilled: () => import('components/elements/icons/baseIcons/svg/filled/notificationFilled'),
    clockFilled: () => import('components/elements/icons/baseIcons/svg/filled/clockFilled'),
    cartFilled: () => import('components/elements/icons/baseIcons/svg/filled/cartFilled'),
    crescentFilled: () => import('components/elements/icons/baseIcons/svg/filled/crescentFilled'),
    walletFilled: () => import('components/elements/icons/baseIcons/svg/filled/walletFilled'),
    walletPlusFilled: () => import('components/elements/icons/baseIcons/svg/filled/walletPlusFilled'),
    quioteFilled: () => import('components/elements/icons/baseIcons/svg/filled/quioteFilled'),

    optionOutline: () => import('components/elements/icons/baseIcons/svg/outline/optionOutline'),
    optionVerticalOutline: () => import('components/elements/icons/baseIcons/svg/outline/optionVerticalOutline'),
    activityOutline: () => import('components/elements/icons/baseIcons/svg/outline/activityOutline'),
    calendarOutline: () => import('components/elements/icons/baseIcons/svg/outline/calendarOutline'),
    cancelOutline: () => import('components/elements/icons/baseIcons/svg/outline/cancelOutline'),
    searchOutline: () => import('components/elements/icons/baseIcons/svg/outline/searchOutline'),
    notificationOutline: () => import('components/elements/icons/baseIcons/svg/outline/notificationOutline'),
    clockOutline: () => import('components/elements/icons/baseIcons/svg/outline/clockOutline'),
    cartOutline: () => import('components/elements/icons/baseIcons/svg/outline/cartOutline'),
    crescentOutline: () => import('components/elements/icons/baseIcons/svg/outline/crescentOutline'),
    walletOutline: () => import('components/elements/icons/baseIcons/svg/outline/walletOutline'),
    walletPlusOutline: () => import('components/elements/icons/baseIcons/svg/outline/walletPlusOutline'),
    quioteOutline: () => import('components/elements/icons/baseIcons/svg/outline/quioteOutline'),

    voiceChatFilled: () => import('components/elements/icons/baseIcons/svg/filled/voiceChatFilled'),
    folderFilled: () => import('components/elements/icons/baseIcons/svg/filled/folderFilled'),
    documentFilled: () => import('components/elements/icons/baseIcons/svg/filled/documentFilled'),
    categoryFilled: () => import('components/elements/icons/baseIcons/svg/filled/categoryFilled'),
    bookFilled: () => import('components/elements/icons/baseIcons/svg/filled/bookFilled'),
    copyFilled: () => import('components/elements/icons/baseIcons/svg/filled/copyFilled'),
    galleyFilled: () => import('components/elements/icons/baseIcons/svg/filled/galleryFilled'),
    imageFilled: () => import('components/elements/icons/baseIcons/svg/filled/imageFilled'),
    editFilled: () => import('components/elements/icons/baseIcons/svg/filled/editFilled'),
    asteriskFilled: () => import('components/elements/icons/baseIcons/svg/filled/asteriskFilled'),
    pauseFilled: () => import('components/elements/icons/baseIcons/svg/filled/pauseFilled'),
    dragFilled: () => import('components/elements/icons/baseIcons/svg/filled/dragFilled'),
    baselineFilled: () => import('components/elements/icons/baseIcons/svg/filled/baselineFilled'),

    voiceChatOutline: () => import('components/elements/icons/baseIcons/svg/outline/voiceChatOutline'),
    folderOutline: () => import('components/elements/icons/baseIcons/svg/outline/folderOutline'),
    documentOutline: () => import('components/elements/icons/baseIcons/svg/outline/documentOutline'),
    categoryOutline: () => import('components/elements/icons/baseIcons/svg/outline/categoryOutline'),
    bookOutline: () => import('components/elements/icons/baseIcons/svg/outline/bookOutline'),
    copyOutline: () => import('components/elements/icons/baseIcons/svg/outline/copyOutline'),
    galleyOutline: () => import('components/elements/icons/baseIcons/svg/outline/galleryOutline'),
    imageOutline: () => import('components/elements/icons/baseIcons/svg/outline/imageOutline'),
    editOutline: () => import('components/elements/icons/baseIcons/svg/outline/editOutline'),
    asteriskOutline: () => import('components/elements/icons/baseIcons/svg/outline/asteriskOutline'),
    pauseOutline: () => import('components/elements/icons/baseIcons/svg/outline/pauseOutline'),
    dragOutline: () => import('components/elements/icons/baseIcons/svg/outline/dragOutline'),
    baselineOutline: () => import('components/elements/icons/baseIcons/svg/outline/baselineOutline'),

    menuDotsFilled: () => import('components/elements/icons/baseIcons/svg/filled/menuDotsFilled'),
    logoutOutline: () => import('components/elements/icons/baseIcons/svg/outline/logoutOutline'),
    downloadOutline: () => import('components/elements/icons/baseIcons/svg/outline/downloadOutline'),
    cloudUploadOutline: () => import('components/elements/icons/baseIcons/svg/outline/cloudUploadOutline'),
    eyeShowOutline: () => import('components/elements/icons/baseIcons/svg/outline/eyeShowOutline'),
    documentEditOutline: () => import('components/elements/icons/baseIcons/svg/outline/documentEditOutline'),
    plusOutline: () => import('components/elements/icons/baseIcons/svg/outline/plusOutline'),
    clipOutline: () => import('components/elements/icons/baseIcons/svg/outline/clipOutline'),
    closeOutline: () => import('components/elements/icons/baseIcons/svg/outline/closeOutline'),
    globeOutline: () => import('components/elements/icons/baseIcons/svg/outline/globeOutline'),
    sortOutline: () => import('components/elements/icons/baseIcons/svg/outline/sortOutline'),
    gallerySkewFilled: () => import('components/elements/icons/baseIcons/svg/filled/gallerySkewFilled'),
    userGroupFilled: () => import('components/elements/icons/baseIcons/svg/filled/userGroupFilled'),

    menuDotsHorizontalFilled: () => import('components/elements/icons/baseIcons/svg/filled/menuDotsHorizontalFilled'),
    loginOutline: () => import('components/elements/icons/baseIcons/svg/outline/loginOutline'),
    downloadDownOutline: () => import('components/elements/icons/baseIcons/svg/outline/downloadDownOutline'),
    cloudDownloadOutline: () => import('components/elements/icons/baseIcons/svg/outline/cloudDownloadOutline'),
    eyeHideOutline: () => import('components/elements/icons/baseIcons/svg/outline/eyeHideOutline'),
    checkOutline: () => import('components/elements/icons/baseIcons/svg/outline/checkOutline'),
    soundOutline: () => import('components/elements/icons/baseIcons/svg/outline/soundOutline'),
    linkOutline: () => import('components/elements/icons/baseIcons/svg/outline/linkOutline'),
    paperPlaneOutline: () => import('components/elements/icons/baseIcons/svg/outline/paperPlaneOutline'),
    currencyEuroOutline: () => import('components/elements/icons/baseIcons/svg/outline/currencyEuroOutline'),
    burgerMenuOutline: () => import('components/elements/icons/baseIcons/svg/outline/burgerMenuOutline'),
    currencyDollarOutline: () => import('components/elements/icons/baseIcons/svg/outline/currencyDollarOutline'),
    userGroupOutline: () => import('components/elements/icons/baseIcons/svg/outline/userGroupOutline'),
    percentOutline: () => import('components/elements/icons/baseIcons/svg/outline/percentOutline'),

    forwardFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/forwardFilled'),
    replyFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/replyFilled'),
    chevronDownOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronDownOutline'),
    chevronRightOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronRightOutline'),
    replyThinOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/replyThinOutline'),
    chevronDoubleDownOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronDoubleDownOutline'),
    chevronDoubleRightOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronDoubleRightOutline'),
    arrowDownOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/arrowDownOutline'),
    arrowRightOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/arrowRightOutline'),
    roundedBottomOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/roundedBottomOutline'),
    roundedRightOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/roundedRightOutline'),
    expandOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/expandOutline'),
    reloadOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/reloadOutline'),

    forwardOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/forwardOutline'),
    replyOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/replyOutline'),
    chevronLeftOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronLeftOutline'),
    chevronUpOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronUpOutline'),
    forwardThinOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/forwardThinOutline'),
    chevronDoubleLeftOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronDoubleLeftOutline'),
    chevronDoubleTopOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronDoubleTopOutline'),
    arrowLeftOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/arrowLeftOutline'),
    arrowUpOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/arrowUpOutline'),
    roundedLeftOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/roundedLeftOutline'),
    roundedUpOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/roundedUpOutline'),
    shrinkOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/shrinkOutline'),
    chevronUpBigOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/chevronUpBigOutline'),

    verticalSortOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/verticalSortOutline'),
    horizontalSortOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/horizontalSortOutline'),
    emojiFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/emojiFilled'),
    paypalFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/paypalFilled'),
    creditCardFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/creditCardFilled'),
    keyOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/keyOutline'),
    karaokeFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/karaokeFilled'),
    finishFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/finishFilled'),
    maleOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/maleOutline'),
    femaleOutline: () => import('components/elements/icons/baseIcons/svg/direction/outline/femaleOutline'),
    discountFilled: () => import('components/elements/icons/baseIcons/svg/direction/filled/discountFilled'),
    dollarOutline: () => import('components/elements/icons/baseIcons/svg/outline/dollarOutline'),
    closeRoundedOutline: () => import('components/elements/icons/baseIcons/svg/outline/closeRoundedOutline'),
    ideaOutline: () => import('components/elements/icons/baseIcons/svg/outline/ideaOutline'),

    appleFilled: () => import('components/elements/icons/baseIcons/svg/filled/appleFilled'),
    androidFilled: () => import('components/elements/icons/baseIcons/svg/filled/androidFilled'),
    nintendo64Filled: () => import('components/elements/icons/baseIcons/svg/filled/nintendo64Filled'),
    switchFilled: () => import('components/elements/icons/baseIcons/svg/filled/switchFilled'),
    playstationFilled: () => import('components/elements/icons/baseIcons/svg/filled/playstationFilled'),
    pcFilled: () => import('components/elements/icons/baseIcons/svg/filled/pcFilled'),
    xboxFilled: () => import('components/elements/icons/baseIcons/svg/filled/xboxFilled'),

    checkRoundedFilled: () => import('components/elements/icons/baseIcons/svg/filled/checkRoundedFilled'),
    closeRoundedFilled: () => import('components/elements/icons/baseIcons/svg/filled/closeRoundedFilled'),

};

export default baseIconset;
