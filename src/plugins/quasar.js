import Vue from 'vue'
import IconsSet from "quasar/icon-set/svg-material-icons"
import { Quasar } from 'quasar'

const config = {
    iconSet: IconsSet
}

Vue.use(Quasar, config)
