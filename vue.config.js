const path = require('path')

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src'),
        'components': path.resolve(__dirname, './src/components'),
      },
    },
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import "~@/styles/quasar.scss";`
      }
    },
    requireModuleExtension: true
  },
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },
  transpileDependencies: [
    'quasar'
  ],
}
